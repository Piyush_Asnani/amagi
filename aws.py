#/usr/bin/python

import boto.ec2
import boto.utils
import time 

instance_name = 'Amagi_Test'
instance_type= 't2.micro'
image_id = 'ami-9abea4fb'
key_name= 'xxxxx'

#Creating a Ubuntu instance from aws 
print('Creating New Machine "'+instance_name+'"...')

conn = boto.ec2.connect_to_region("us-west-2")
myinstance = conn.run_instances(
    image_id = image_id,
    key_name = key_name,
    instance_type = instance_type,
    security_groups = ['amagi']
)

instance = myinstance.instances[0]
conn.create_tags([instance.id], {"Name": instance_name})

print('Waiting for instance to start...')
# Check up on its status every 5 sec
status = instance.update()
while status == 'pending':
	time.sleep(5)
	status = instance.update()
	print str(status) + '...'
if status == 'running':
	print('New instance "' + instance.id + '" is running... waiting for complete start.')
        print('Public Ip Address of newly created instance is "' + instance.ip_address + '" ')	
