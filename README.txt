aws.py -- This script will launch a new instance and will return the ip address of the instance .
Create a security group '****' and key_name'****' and change the script accordingly before triggering the script. 

Since the script uses boto framework .Create a file ~/.boto with below contents.
[Credentials]
aws_access_key_id = xxxxx
aws_secret_access_key = xxxxxxxxxxxxxxxxx 

sql.py - This script will create the sql DB having text and Title field used by the blogging app.

blog.py - This script will create the blogging application.

project.yml - Ansible project file .

ansible_hosts - Update this file with the ip address coming from aws.py before starting the ansible project.
